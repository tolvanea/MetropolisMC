import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os
#import natural_constants_SI as nc

from matplotlib.colors import LinearSegmentedColormap
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection


"""
This file contains everything that relate to plotting of results. 
(Code is somewhat spaghtetti-script)

Some of functions in this file are modified from earlier projects, so they are 
not fully made in course RAK-19006. Those functions have been notated with :
"### From earlier projects, not fully made in made in RAK-19006. ###"
"""


def plot_walkers(results, name, plot_3d_path=False):
    """
    Plot varying information about walkers in one figure.
    - electron density
    - acceptance probability (methropolis algorithm works best at 0.3-0.6)
    - polarization
    - trace of walker paths in 3d-space.
    """
    
    # create plots with uneven partitioning in figure
    fig = plt.figure(figsize=(15,7), num=name)
    ax1 = plt.subplot2grid((2, 4), (0, 0), colspan=2, rowspan=1)
    ax2 = plt.subplot2grid((2, 4), (1, 0), colspan=1, rowspan=1)
    ax3 = plt.subplot2grid((2, 4), (1, 1), colspan=1, rowspan=1)
    ax4 = plt.subplot2grid((2, 4), (0, 2), colspan=2, rowspan=2, projection='3d')
    
    density_map = results[0].electron_density_mean.copy()
    
    # sum walkers together
    for i, result in enumerate(results):
        plot_vector_of_walk(ax2, results[i].accept_prob, "Accept probability", i)
        plot_vector_of_walk(ax3, results[i].P4, "Polarization, cell 4", i)
        
        if i != 0:
            density_map += results[i].electron_density_mean
    
    l_x = results[0].conf.dens_dimensions[0]
    l_y = results[0].conf.dens_dimensions[1]
    plot_el_density_and_QD_circles(ax1, density_map, l_x, l_y, results[0].QD_positions)
    
    scatter_3d(results, name=name, ax=ax4)
    
    decorate_figures(ax1, ax2, ax3, ax4)
    
    # Bug in my anaconda-matplotlib!, tight_layout does not work properly!
    #fig.tight_layout(pad=0.0) 
    fig.subplots_adjust(hspace=.3)
    fig.suptitle(name, fontsize=14)
    plt.subplots_adjust(top=0.9)
    
    if not os.path.exists("images"):
        os.makedirs("images")
    fig.savefig("images" + os.path.sep + name)
    
    #plt.show()


def plot_vector_of_walk(ax, vec, title, i):
    
    x_axis = np.arange(len(vec))
    
    ax.plot(x_axis, vec, label="walker {}".format(i))
    
    ax.set_xlabel("Walker sample")
    ax.set_title(title)    
    
    
def decorate_figures(ax1, ax2, ax3, ax4):
    
    ax2.set_ylim([ -0.02, +1.02])
    ax3.set_ylim([-1.02, +1.02])
    
    # already done in 'scatter_3d()':
    #ax4.set_ylim([-1, +1])  
    #ax4.plot([-10,-10,-10], [10,10,10])
    #ax4.set_aspect('equal')

    ax1.set_title("Electron density")
    ax1.set_xlabel("x (nm)")
    ax1.set_ylabel("y (nm)")
    
    ax2.set_title("Acceptance probability")
    ax2.set_xlabel("Sample number (walker over time)")
    #ax2.set_ylabel("Prob")
        
    ax3.set_title("Polarization Cell 4")
    ax3.set_xlabel("Sample number (walker over time)")
    #ax3.set_ylabel("Pol")
    
    ax4.set_title("Walker paths drawn in 3d-space")
    ax4.set_xlabel("x")
    ax4.set_ylabel("y")
    ax4.set_ylabel("z")
    
        
### From earlier projects, not fully made in made in RAK-19006. ###
def plot_el_density_and_QD_circles(ax, density_map, res_x, res_y, QDs):
    
    min_x, max_x, min_y, max_y = get_lims(QDs)
    
    den_grid_x = np.linspace(-res_x/2, res_x/2, density_map.shape[0])
    den_grid_y = np.linspace(-res_y/2, res_y/2, density_map.shape[1])
    Z = np.sum(density_map, axis=2)
    Z = np.rot90(Z,1)
    Z = np.flipud(Z)
    
    # tune linewidth (somewhat) proportionally
    diag = np.sqrt((max_x - min_x)**2 + (max_y - min_y)**2)
    linewidth = np.sqrt(9 / diag) * 1.5

    plot_density_remote(ax, Z, den_grid_x, den_grid_y)

    # ASSUMING FIRST CELL IS DRIVER! (incorrect plot can be seen by eye)
    plot_quantum_dots(QDs[4:, ...], QDs[:4, ...], linewidth=linewidth, ax=ax)
    
    # include some text-informatio in picture
    add_text(ax)
    
    if ax is None:
        plt.axis((min_x-1, max_x+1, min_y-2, max_y+2))
        plt.xlabel("x (nm)")
        plt.ylabel("y (nm)")
    else:
        ax.set_xlim([min_x-1, max_x+1])
        ax.set_ylim([min_y-1, max_y+1])
        ax.set_xlabel("x (nm)")
        ax.set_ylabel("y (nm)")
        

### From earlier projects, not fully made in made in RAK-19006. ###        
def get_lims(QDs):
    """
    Gets the limits for the plot, takes it from the quantum dots that are 
    furthest apart
    """
    max_x = -9999
    max_y = -9999
    min_x = 9999
    min_y = 9999

    for prt in QDs:
        max_x = max(max_x, prt[0])
        max_y = max(max_y, prt[1])
        min_x = min(min_x, prt[0])
        min_y = min(min_y, prt[1])

    return min_x, max_x, min_y, max_y
        

### From earlier projects, not fully made in made in RAK-19006. ###
def colormap_black_to_blue():
    # My own colormap from black to blue to half-white
    # derivate of color is normalized to 2. meaning (d(r+g+b) / dx) = 2
    
    """
    https://matplotlib.org/examples/pylab_examples/custom_cmap.html
    row i:   x  y0  y1
                   /
                  /
    row i+1: x  y0  y1

    Above is an attempt to show that for x in the range x[i] to x[i+1], the
    interpolation is between y1[i] and y0[i+1].  So, y0[0] and y1[-1] are
    never used.
    """
    cdict = {  'red':      ((0.0, 0.0, 0.0),
                            (0.2, 0.1, 0.1),
                            (0.6, 0.15, 0.15),
                            (1.0, 0.55, 0.55)),

                'green':   ((0.0, 0.0, 0.0),
                            (0.2, 0.1, 0.1),
                            (0.6, 0.15, 0.15),
                            (1.0, 0.55, 0.55)),

                'blue':    ((0.0, 0.0, 0.0),
                            (0.2, 0.3, 0.3),
                            (0.6, 1.0, 1.0),
                            (1.0, 1.0, 1.0))
                }
    
    black_blue = LinearSegmentedColormap('BlackBlue', cdict)
    #interpolation='nearest', cmap=blue_red1
    return black_blue


### From earlier projects, not fully made in made in RAK-19006. ###
def plot_density_remote(ax, Z, den_grid_x, den_grid_y):
    """
    Plots electron density background on top of which QDs are drawn.
    """

    lims = [den_grid_x[0], den_grid_x[-1], den_grid_y[0], den_grid_y[-1]]
    #ax.imshow(Z, extent=lims, origin='lower', cmap=cm.inferno)
    ax.imshow(Z, extent=lims, origin='lower', interpolation='nearest',
              cmap=colormap_black_to_blue())
    
def add_text(ax):
    items = ((-10, -2.6, "↑ driver cell", 'left'),
             (-10, -3.0, "(2 pos. 2 neg. QD:s", 'left'),
             (-10, -3.4, "(Quantum Dots))", 'left'),
             (-5, -2.6, "↑ A quantum dot cell ", 'left'),
             (-5, -3.0, "of four positive QD:s", 'left'),
             (-5, -3.4, "(hopefully circles show up ,", 'left'),
             (-5, -3.8, "hollow (anaconda works))", 'left'),
             (4, -2.6, "Electrons are in opposing ", 'left'),
             (4, -3.0, "corners leading forward ", 'left'),
             (4, -3.4, "→ polarization $P=+1$", 'left'),
             (-4.5, 2.4, "cell 1", 'center'),
             ( 0,   2.4, "cell 2", 'center'),
             ( 4.5, 2.4, "cell 3", 'center'),
             ( 9,   2.4, "cell 4", 'center')
             )
    for x, y, s, ha in items:
        ax.text(x, y, s, color="darkgray", verticalalignment="center",
                horizontalalignment=ha, fontsize=6)

### From earlier projects, not fully made in made in RAK-19006. ###
def plot_quantum_dots(particles, fixed_particles, linewidth=1, ax=None):

    # new arrangement      0     1     2      3      4
    # particle np.array [crd1, crd2, crd3, charge, radius]
    
    # Driver QDs are now separate, and plotted as filled circles
    pos_qdots_driver = []
    neg_qdots_driver = []
    
    for prt in fixed_particles:
        if (-0.6 < prt[3] < -0.0):
            neg_qdots_driver.append(prt)
        elif (0.0 < prt[3] < 0.6):
            pos_qdots_driver.append(prt)
        elif (-0.001 < prt[3] < 0.001):
            pass
        else:
            raise Exception("Unknown particle in plotting, properties [crd1, crd2, crd3, charge, radius] = ", 
                            prt, ". \nNotice that charges are expected to be on range -0.5e - +0.5e." +
                            " If not, modify the code here.")

    electrons=[]
    pos_qdots=[]
    neg_qdots=[]
    
    for prt in particles:
        if (-1.1 < prt[3] < -0.9) and (prt[4] < 0.001):
            electrons.append(prt)
        elif (-0.6 < prt[3] < -0.0):
            neg_qdots.append(prt)
        elif (0.0 < prt[3] < 0.6):
            pos_qdots.append(prt)
        elif (-0.01 < prt[3] < 0.01):
            pass
        else:
            raise Exception(
                "Unknown particle in plotting, properties [crd1, crd2, crd3, charge, radius] = ", 
                prt, ". Notice that charges are expected to be on range -1e - +1e. If not, modify this code.")
        
    x_pos_qdots = [p_qdot[0] for p_qdot in pos_qdots]
    y_pos_qdots = [p_qdot[1] for p_qdot in pos_qdots]
    x_pos_qdots_driver = [p_qdot[0] for p_qdot in pos_qdots_driver]
    y_pos_qdots_driver = [p_qdot[1] for p_qdot in pos_qdots_driver]
    # radius_pos = pos_qdots[0][4]
    # radiai_pos = [radius_pos for i in range(len(pos_qdots))]
    radiai_pos = [pQD[4] for pQD in pos_qdots]
    radiai_pos_driver = [pQD[4] for pQD in pos_qdots_driver]

    x_neg_qdots = [n_qdot[0] for n_qdot in neg_qdots]
    y_neg_qdots = [n_qdot[1] for n_qdot in neg_qdots]
    x_neg_qdots_driver = [n_qdot[0] for n_qdot in neg_qdots_driver]
    y_neg_qdots_driver = [n_qdot[1] for n_qdot in neg_qdots_driver]
    radiai_neg = [nQD[4] for nQD in neg_qdots]
    radiai_neg_driver = [nQD[4] for nQD in neg_qdots_driver]

    x_els = [el[0] for el in electrons]
    y_els = [el[1] for el in electrons]
    radiai_el = [0.1 for i in range(len(electrons))]
    
    # Following 'circles' function does not work properly with some versions of matplotlib! 
    # (Circles should be rendered hollow, anaconda version seems to work.)
    out = circles(x_pos_qdots, y_pos_qdots, radiai_pos, ax=ax, c='r', fc='none', lw=linewidth)
    out = circles(x_neg_qdots, y_neg_qdots, radiai_neg, ax=ax, c='b', fc='none', lw=linewidth)
    
    out = circles(x_pos_qdots_driver, y_pos_qdots_driver, radiai_pos_driver, ax=ax, c='r', fc='r', lw=linewidth)
    out = circles(x_neg_qdots_driver, y_neg_qdots_driver, radiai_neg_driver, ax=ax, c='b', fc='b', lw=linewidth)
    #out = circles(x_els, y_els, radiai_el, ax=ax, color=(0,0,1))

    if ax is None:
        plt.axis('equal')
    else:
        ax.axis('equal')

    min_x, max_x, min_y, max_y = get_lims(particles)
    
    if (max_y-min_y) < 12:
        if ax is None:
            plt.axis([min_x, max_x, -7, 7])
        else:
            ax.axis([min_x, max_x, -7, 7])
            

### From earlier projects, not fully made in made in RAK-19006. ###
# Also this function is not made by me, and it is from
# https://stackoverflow.com/questions/9081553/python-scatter-plot-size-and-style-of-the-marker/24567352#24567352
def circles(x, y, s, c='b', ax=None, vmin=None, vmax=None, **kwargs):
    """
    Make a scatter of circles plot of x vs y, where x and y are sequence
    like objects of the same lengths. The size of circles are in data scale.

    Parameters
    ----------
    x,y : scalar or array_like, shape (n, )
        Input data
    s : scalar or array_like, shape (n, )
        Radius of circle in data unit.
    c : color or sequence of color, optional, default : 'b'
        `c` can be a single color format string, or a sequence of color
        specifications of length `N`, or a sequence of `N` numbers to be
        mapped to colors using the `cmap` and `norm` specified via kwargs.
        Note that `c` should not be a single numeric RGB or RGBA sequence
        because that is indistinguishable from an array of values
        to be colormapped. (If you insist, use `color` instead.)
        `c` can be a 2-D array in which the rows are RGB or RGBA, however.
    vmin, vmax : scalar, optional, default: None
        `vmin` and `vmax` are used in conjunction with `norm` to normalize
        luminance data.  If either are `None`, the min and max of the
        color array is used.
    kwargs : `~matplotlib.collections.Collection` properties
        Eg. alpha, edgecolor(ec), facecolor(fc), linewidth(lw), linestyle(ls),
        norm, cmap, transform, etc.

    Returns
    -------
    paths : `~matplotlib.collections.PathCollection`

    Examples
    --------
    a = np.arange(11)
    circles(a, a, a*0.2, c=a, alpha=0.5, edgecolor='none')
    plt.colorbar()

    License
    --------
    This code is under [The BSD 3-Clause License]
    (http://opensource.org/licenses/BSD-3-Clause)
    """
    if np.isscalar(c):
        kwargs.setdefault('color', c)
        c = None
    if 'fc' in kwargs: kwargs.setdefault('facecolor', kwargs.pop('fc'))
    if 'ec' in kwargs: kwargs.setdefault('edgecolor', kwargs.pop('ec'))
    if 'ls' in kwargs: kwargs.setdefault('linestyle', kwargs.pop('ls'))
    if 'lw' in kwargs: kwargs.setdefault('linewidth', kwargs.pop('lw'))

    patches = [Circle((x_, y_), s_) for x_, y_, s_ in np.broadcast(x, y, s)]
    collection = PatchCollection(patches, **kwargs)
    if c is not None:
        collection.set_array(np.asarray(c))
        collection.set_clim(vmin, vmax)

    ax_old = plt.gca()
    if ax is None:
        ax = ax_old
    else:
        """
        from matplotlib import _pylab_helpers, interactive
        managers = _pylab_helpers.Gcf.get_all_fig_managers()
        for m in managers:
            print("set axes: ", ax, ax_old)
            for Ax in m.canvas.figure.axes:
                print("iterate axes: ", Ax)
                if (ax==Ax):
                    print("Right here is a bug of 'sca()' and 'subplot2grid()'")
        """
        plt.sca(ax)
        
    ax.add_collection(collection)
    ax.autoscale_view()
    if c is not None:
        plt.sci(collection)
    
    plt.sca(ax_old)
    
    return collection


# Using following code as a help:
# https://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html
def draw_sphere(ax, x_pos, y_pos, z_pos, R, color='b'):
    
    # Make data
    res = 30
    u = np.linspace(0, 2 * np.pi, res)
    v = np.linspace(0, np.pi, res)
    x = np.outer(R * np.cos(u),    R * np.sin(v)) + x_pos
    y = np.outer(R * np.sin(u),    R * np.sin(v)) + y_pos
    z = np.outer(R * np.ones(res), R * np.cos(v)) + z_pos

    # Plot the surface
    ax.plot_surface(x, y, z, color='b', alpha=0.2)

    plt.show()


# def density_3d():
#     from scipy import stats
#     from mayavi import mlab
#
#     mu, sigma = 0, 0.1
#     x = 10*np.random.normal(mu, sigma, 5000)
#     y = 10*np.random.normal(mu, sigma, 5000)
#     z = 10*np.random.normal(mu, sigma, 5000)
#
#     xyz = np.vstack([x,y,z])
#     kde = stats.gaussian_kde(xyz)
#
#     # Evaluate kde on a grid
#     xmin, ymin, zmin = x.min(), y.min(), z.min()
#     xmax, ymax, zmax = x.max(), y.max(), z.max()
#     xi, yi, zi = np.mgrid[xmin:xmax:30j, ymin:ymax:30j, zmin:zmax:30j]
#     coords = np.vstack([item.ravel() for item in [xi, yi, zi]])
#     density = kde(coords).reshape(xi.shape)
#
#     # Plot scatter with mayavi
#     figure = mlab.figure('DensityPlot')
#
#     grid = mlab.pipeline.scalar_field(xi, yi, zi, density)
#     min = density.min()
#     max=density.max()
#     mlab.pipeline.volume(grid, vmin=min, vmax=min + .5*(max-min))
#
#     mlab.axes()
#     mlab.show()

    
    
    
def scatter_3d(results, name="", ax=None):
    """
    (DEBUG function)
    Plot walker path id 3d-space
    """
    
    if (ax == None):
        fig = plt.figure(num = "DEBUG_PATH_" + name)
        ax4 = fig.add_subplot(111, projection='3d')
    
    # electron walker paths
    for i, result in enumerate(results):
        els = results[i].electrons_pos
        
        for i in range(els.shape[1]):
            xs = els[:,i, 0]
            ys = els[:,i, 1]
            zs = els[:,i, 2]
            ax.plot(xs, ys, zs, color="C{}".format(i))
            
    # Turn True to plot markers on QD positions
    #       - point-markers work, (but are ugly)
    #       - 3d-spheres seems not to render properly in my version of matplotlib
    if False:

        QDs = results[0].QD_positions
        for i in range(QDs.shape[0]):
            x = QDs[i,0]
            y = QDs[i,1]
            z = QDs[i,2]
            
            if QDs[i,3] < 0:
                QD_col = "b"
            else:
                QD_col = "r"
            
            # Draw 3d-sphere?  warning: DAMIT MATPLOTLIB-3D IS BUGGY
            if False:
                draw_sphere(ax, x, y, z, QDs[i,4], color=QD_col)
            else: # mark QDs without transparent sphere (bugs)
                ax.scatter([x], [y], [z],          marker="x", color=QD_col)
                ax.scatter([x], [y], [z+QDs[i,4]], marker="^", color=QD_col)
                ax.scatter([x], [y], [z-QDs[i,4]], marker="v", color=QD_col)
        
    min_x, max_x, min_y, max_y = get_lims(results[0].QD_positions)
    ax.set_xlim3d(min_x+4.5, max_x+0.5)
    ax.set_ylim3d(min_x+2, max_x-2)
    ax.set_zlim3d(min_x+2, max_x-2)
    
    
    

