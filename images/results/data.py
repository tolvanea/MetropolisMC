"""
This file plots a bar chart about running time results.

Used following code as a base:
https://matplotlib.org/gallery/statistics/barchart_demo.html
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from collections import namedtuple


names_times = [("1. python", 135.52), ("2. thread pool", 47.19), 
               ("3. numba", 3.35), ("4. cython", 4.68), ("5.rust", 5.61), 
               ("6: Numpy", 79.95)]
names = [nm[0] for nm in names_times]
times = [nm[1] for nm in names_times]

n_groups = len(names_times)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.6

opacity = 0.4
error_config = {'ecolor': '0.3'}

rects1 = ax.bar(index + 0.5*bar_width, times, bar_width,
                alpha=opacity, color='b')

#ax.set_xlabel('Method')
ax.set_ylabel('Time (sec)')
ax.set_title("Running times of optimizations of 'total_energy()'")
ax.set_xticks(index + bar_width / 2)
ax.set_xticklabels(names)

fig.tight_layout()
print("WTF=??")
fig.savefig("compare_running_times.png")
plt.show()

