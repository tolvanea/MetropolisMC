#![crate_type = "dylib"]

///
/// This library file compiles dynamically linked energy-function, which can be used with 
/// used with pythons c-interface.
///

use ndarray::{Array, Array2};
// #[macro_use(stack)]
extern crate ndarray;

use std::mem;


/// Take raw pointer and convert it to 2d ndarray.
pub unsafe fn construct_array_2d<T>(input: *mut T, m: usize, n: usize) -> Array2<T>
{
    assert!(!input.is_null());
    let v = Vec::from_raw_parts(input, m*n, m*n);
    Array::from_shape_vec((m,n), v).unwrap()
}


/// My function to concatenate two arrays into one. (It stacks by first dimension.)
fn stack_em_goddamit(els: &Array2<f64>, QDs: &Array2<f64>) -> Array2<f64>
{
    let mut concat = Array2::zeros((els.dim().0 + QDs.dim().0, els.dim().1));
    for ((i,j), element) in concat.indexed_iter_mut(){
        if (i < els.dim().0){
            *element = els[[i, j]];
        }
        else{
            *element = QDs[[i-els.dim().0, j]];
        }
    }
    return concat;
}


/// Calculate the total energy of system consisting of electrons and Quantum dots.
pub fn total_energy(els: &Array2<f64>, QDs: &Array2<f64>) -> f64
{
    let eps0 : f64 = 8.854187817e-12;
    let pi : f64 = 3.1415926535;
    let dielectric: f64 = 1.0;
    let k : f64 = 1.0/(4.0*pi*eps0*dielectric);
    let q_e : f64 = 1.60217662e-19;
    
    let num_els = els.dim().0;
    let num_QDs = QDs.dim().0;
    
    let mut Et = 0.0;
    
    // DANGIT! Theres no rust alternative to python array stacking:
    //              >>> A = np.concatenate((els, QDs), axis=0)
    // let A = stack![Axis(0), (&els, &QDs)];  // <-- does not work, idk why
    let A = stack_em_goddamit(els, QDs);       // <-- I had to make my own function
    
    for i in 0..num_els{  // skip all QD-QD -interactions (=constant)
        for j in (i+1)..num_els+num_QDs{
            let dist = ((A[[i,0]]-A[[j,0]]).powi(2) + 
                        (A[[i,1]]-A[[j,1]]).powi(2) + 
                        (A[[i,2]]-A[[j,2]]).powi(2)).sqrt();
            

            let larger_radius = if (A[[i,4]] < A[[j,4]]) {A[[j,4]]} else {A[[i,4]]};
            
            if (dist < larger_radius){
                Et += k * A[[i,3]] * A[[j,3]] / (2.0 * larger_radius) * 
                      (3.0 - dist.powi(2)/larger_radius.powi(2)) * (q_e.powi(2) / (1e-9));
            }
            else{
                Et += k * A[[i,3]] * A[[j,3]] / 
                      (dist) * (q_e.powi(2) / (1e-9));  // last term is for unit
            }
        }
    }
        
    return Et;
}


/// This function converts raw pointers to rust ndarrays and then passes them
/// to further calculations.
#[no_mangle]
pub extern fn rust_entry_function(v1_ptr: *mut f64, v1_m: usize, v1_n: usize,
                                  v2_ptr: *mut f64, v2_m: usize, v2_n: usize) -> f64 
{
    // Build immutable input arrays from raw parts.
    let els = unsafe { construct_array_2d(v1_ptr, v1_m, v1_n) };
    let QDs = unsafe { construct_array_2d(v2_ptr, v2_m, v2_n) };
    
    let E = total_energy(&els, &QDs);
    
    mem::forget(els);  // do no free the memory of numpy arrays
    mem::forget(QDs);
    
    return E;
}


