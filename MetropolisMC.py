import numpy as np

import utilities
import plotting


"""
This file contains 3 data-containers (which have some associated methods)
    - ConfigurationParameters
    - State
    - Observables
"""

class ConfigurationParameters():
    """Configuration parameters of system. None of these values should be changed
    after initialization.
    """
    def __init__(self):
        self.num_samples = 1000    # CHANGE THIS FOR BETTER ACCUARY
        self.num_moves = 30       # sample taking frequency out of random walker
        self.convergence = 90     # throw away first BXX samples
        
        self.QD_radius = 0.5      # (nm)
        self.a = 1                # (nm),  2*a = cell_size = QD separation
        self.d = 4.5              # (nm), d = cell_separation 
                                  #       --> spacing of QD:s between cells is 2.5 nm.
                               
        self.dielectric = 1.0     # (relative)
        self.T = 300              # (K) Temperature
        
        self.dens_resolution   = 8    # pixels per nanometer in density
        self.dens_dimensions = (32.0, 4.0, 2.0) # density length box (nm)

        self.rnd_move_length = self.QD_radius * 0.5
        self.leap_prob = 0.1
        
        
class State():
    """Stores the state of system. Contains information of electorn positions for example.
    """
    def __init__(self, c):
        """
        Args
            c : copy of ConfigurationParameters
        """
        
        self.conf = c
        
        # 4*2 electrons in starting positions
        #                     |------------- ---- ---- ------- ------|
        # indices of particle:[0,            1,   2,   3,      4     ]
        # description of idx: [x,            y,   z,   charge, radius]
        self.els = np.array([ # (1st cell:)
                              [ -c.d + c.a, -c.a, 0.0, -1.0,   0.0   ],
                              [ -c.d - c.a,  c.a, 0.0, -1.0,   0.0   ], 
                              # (2nd cell)
                              [0*c.d + c.a, -c.a, 0.0, -1.0,   0.0   ],   
                              [0*c.d - c.a,  c.a, 0.0, -1.0,   0.0   ],
                              # (3rd cell)
                              [1*c.d + c.a, -c.a, 0.0, -1.0,   0.0   ],
                              [1*c.d - c.a,  c.a, 0.0, -1.0,   0.0   ],
                              # (4th cell)
                              [2*c.d + c.a, -c.a, 0.0, -1.0,   0.0   ],
                              [2*c.d - c.a,  c.a, 0.0, -1.0,   0.0   ]])  
        #                     [x,            y,   z,   charge, radius]
        #                     |------------- ---- ---- ------- ------|
        
        self.els_tmp = np.full_like(self.els, fill_value=float("nan"))
                                
        
        self.cell_center_points = np.array([[ -c.d, 0.0, 0.0], 
                                            [0*c.d, 0.0, 0.0],
                                            [1*c.d, 0.0, 0.0],
                                            [2*c.d, 0.0, 0.0]])
        # Quantum dot array (driver + 4 * 4 quantum dots)
        #                   [x,             y,   z,    charge, radius  ]
        self.QDs = np.array([# driver cell:
                            [-c.a - 2*c.d,  c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a - 2*c.d,  c.a, 0.0, -0.5, c.QD_radius],
                            [ c.a - 2*c.d, -c.a, 0.0,  0.5, c.QD_radius],
                            [-c.a - 2*c.d, -c.a, 0.0, -0.5, c.QD_radius],
                            # 1st cell:
                            [-c.a - c.d,    c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a - c.d,    c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a - c.d,   -c.a, 0.0,  0.5, c.QD_radius],
                            [-c.a - c.d,   -c.a, 0.0,  0.5, c.QD_radius],
                            # 2nd cell:
                            [-c.a,          c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a,          c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a,         -c.a, 0.0,  0.5, c.QD_radius],
                            [-c.a,         -c.a, 0.0,  0.5, c.QD_radius],
                            # 3rd cell:
                            [-c.a + 1*c.d,  c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a + 1*c.d,  c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a + 1*c.d, -c.a, 0.0,  0.5, c.QD_radius],
                            [-c.a + 1*c.d, -c.a, 0.0,  0.5, c.QD_radius],
                            # 4th cell:
                            [-c.a + 2*c.d,  c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a + 2*c.d,  c.a, 0.0,  0.5, c.QD_radius],
                            [ c.a + 2*c.d, -c.a, 0.0,  0.5, c.QD_radius],
                            [-c.a + 2*c.d, -c.a, 0.0,  0.5, c.QD_radius]
                            ])
        
        self.num_els   = len(self.els)      # number of electrons (2_per cell)
        
        self.num_cells = round(len(self.QDs)/4) - 1  # number of cells (not including driver)
        
        if (self.num_els != 8) or (self.num_cells != 4):
            print("self.num_els", self.num_els, ",   self.num_cells", self.num_cells)
            raise Exception("Four cells are hard coded in 'State'. This program is " + 
                            "just a quick throw-it-together, not a flexible simulator.")
        
    
    def make_move(self):
        """Moves one or two electrons by normal distribution. Also large
        leap-moves are included, and they are designed to move electron from 
        quantum dot to another.
        """
        self.els_tmp = self.els.copy()
        
        # normal move
        if np.random.rand() > self.conf.leap_prob:
            rnd_particle_idx = np.random.randint(np.shape(self.els_tmp)[0])
            
            # gaussian move
            rnd_move = np.random.normal(scale=self.conf.rnd_move_length, size=3)  
            self.els_tmp[rnd_particle_idx, 0:3] += rnd_move
            
        # Do a rare (and large) leap, in which two electrons are moved quantum dot 
        # separation distances.
        else:
            rnd_move_1 = np.random.normal(scale=self.conf.rnd_move_length/8, size=3)
            rnd_move_2 = np.random.normal(scale=self.conf.rnd_move_length/8, size=3)
            
            # increase or decrease by one
            sign1 = 1 - 2*np.random.randint(2)
            sign2 = 1 - 2*np.random.randint(2)
            # idx x or y (left/right or up/down)
            idx_1 = np.random.randint(2)
            idx_2 = np.random.randint(2)
            
            rnd_move_1[idx_1] += sign1 * 2*self.conf.a
            rnd_move_2[idx_2] += sign2 * 2*self.conf.a
                
            num_prt = np.shape(self.els_tmp)[0]
            sign0 = 1 - 2*np.random.randint(2)
            # rnd_particle_idx
            prt_idx_1 = np.random.randint(num_prt)
            prt_idx_2 = max(0, min(num_prt-1, prt_idx_1 + sign0))
            
            self.els_tmp[prt_idx_1, 0:3] += rnd_move_1
            self.els_tmp[prt_idx_2, 0:3] += rnd_move_2
            
        return self.els_tmp
    

class Observables():
    """Container for observabled values that are calculated and updated on the
    simulatioin
    """
    def __init__(self, conf, state):
        if (state.num_cells != 4):
            raise Exception("Four cells are hard coded in 'Observables'.")
        
        # Vectors containing observable-samples
        # Polarisation of cell 1
        self.P1 = np.full(shape=conf.num_samples, fill_value=float("nan"))
        self.P4 = np.full(shape=conf.num_samples, fill_value=float("nan"))
        self.electrons_pos = np.full((conf.num_samples, *state.els.shape), 
                                     fill_value=float("nan"))
        self.accept_prob = np.full(shape=conf.num_samples, fill_value=float("nan")) 
        
        # reduced observables in the end of simulation, (normalized)
        self.P1_mean = float("nan")  # (btw, polarisation indicates the arrangement of electrons)
        self.P4_mean = float("nan")
        grid = [int(d * conf.dens_resolution + 1) for d in conf.dens_dimensions]
        self.electron_density_mean = np.full(grid, fill_value=float("nan"))
        self.accept_prob_mean = float("nan")
        
        # temporary values
        self.accept_counter = 0  # number of accepts in 'num_moves'
        self.update_counter = 0  # number of updates out of 'num_samples'
        
        # copy (or actually reference to) configuration
        self.conf = conf  
        # also, take up electron count and quantum dot positions
        self.num_els = state.num_els
        self.QD_positions = state.QDs
                
    def update(self, state):
        """
        Updates the observables to a sample after 'conf.num_moves' random moves.
        """
        
        P = utilities.cell_polarizations(state, self.conf)
        
        self.P1[self.update_counter] = P[0]
        self.P4[self.update_counter] = P[3]
        self.electrons_pos[self.update_counter, :, :] = state.els
        self.accept_prob[self.update_counter] = self.accept_counter / self.conf.num_moves
        self.accept_counter = 0
        self.update_counter += 1
            
    def reduce_mean(self, state):
        """Calculate mean from all samples of full walker path.
        """
        if(self.update_counter != self.conf.num_samples):
            raise Exception("Calculation is not yet finished!")
        
        self.P1_mean = np.mean(self.P1[self.conf.convergence:])
        self.P4_mean = np.mean(self.P4[self.conf.convergence:])
        self.calc_dens()
        self.accept_prob_mean = np.mean(self.accept_prob[self.conf.convergence:])
        
    def calc_dens(self):
        """Calclulate 3d electron density from array of coordinates
        """
        
        def get_idx(x, l, res_x) -> int:
            """ Converts a spacial coordinate to index in density-map"""
            r = l/2 + x  # shift range of : (-l/2) - (l/2)  ---> r : 0 - l
            
            if (r < 0) or (r >= l):
                return -1
            else:
                return int((r / (l)) * res_x)
        
        eps = 1 / (self.conf.num_samples * self.num_els)
        
        self.electron_density_mean = np.full_like(self.electron_density_mean, fill_value=0.0)
        
        res = self.conf.dens_resolution
        parms = [(k, l, l * res) for k, l in enumerate(self.conf.dens_dimensions)]  # index, scaling factors

        for sample in self.electrons_pos:
            for el in sample:
                ix, iy, iz = [get_idx(el[k], l, lr) for k, l, lr in parms]

                if all(m >= 0 for m in (ix,iy,iz)):  # electron inside the density map?
                    self.electron_density_mean[ix, iy, iz] += eps
                
    
def calc_prob(E_diff, conf):
    """
    Calculate acceptanse probability of move from energy difference.
    """
    k_B = 1.38064852e-23 # Boltzmanns constant
    
    if E_diff < 0:
        return 1
    else:
        return np.exp(- E_diff / (k_B * conf.T))
            

def metropolis_loop(state, conf, energy_func, seed=1):
    """The main metropolis algorithm loop of one walker.
    """

    np.random.seed(seed)
    
    obs = Observables(conf, state)
    
    for sample_idx in range(conf.num_samples):

        energy_prev = energy_func(state.els, state.QDs)
        
        for move_idx in range(conf.num_moves):
            
            proposed_state = state.make_move()
            energy_new = energy_func(proposed_state, state.QDs)
            accept_prob = calc_prob(energy_new - energy_prev, conf)

            if np.random.rand() <= accept_prob:  # accept proposed state
                state.els = proposed_state
                energy_prev = energy_new
                obs.accept_counter += 1
        
        obs.update(state)

    obs.reduce_mean(state)
    
    return obs

