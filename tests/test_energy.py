import energy
import energy_cython
import unittest
import numpy as np
import numba as nb
import subprocess
import rust_caller



class TestEnergy(unittest.TestCase):

    def test_simple_system(self):
        self.compare_energy_functions(*self.generate_simple_system())

    def test_random_systems(self):
        for i in range(10):
            self.compare_energy_functions(*self.generate_random_system())

    def compare_energy_functions(self, els, QDs):
        E_normal = energy.total_energy(els, QDs)

        energy_jit = nb.jit(nb.double(nb.double[:, :], nb.double[:, :]),
                            nopython=True)(energy.total_energy)
        E_numba = energy_jit(els, QDs)

        subprocess.run(['python3', 'setup.py', 'build_ext', '--inplace'],
                       stdout=subprocess.PIPE)
        E_cython = energy_cython.total_energy(els, QDs)

        caller = rust_caller.RustEnergyCaller()
        E_rust = caller.run(els, QDs)
        E_vectorized = energy.total_energy_vectorized(els, QDs)

        Es = np.array([E_normal, E_numba, E_cython, E_rust, E_vectorized])
        differences = Es - E_normal

        self.assertTrue(np.sum(differences) < 0.00001)

    def generate_simple_system(self):

        class c:
            QD_radius = 0.5  # (nm)
            a = 1  # (nm),  2*a = cell_size = QD separation
            d = 4.5  # (nm), d = cell_separation

        # indices of particle:[0,            1,   2,   3,      4     ]
        # description of idx: [x,            y,   z,   charge, radius]
        els = np.array([  # (1st cell:)
            [-c.d + c.a, -c.a, 0.0, -1.0, 0.0],
            [-c.d - c.a, c.a, 0.0, -1.0, 0.0],
            # (2nd cell)
            [0 * c.d + c.a, -c.a, 0.0, -1.0, 0.0],
            [0 * c.d - c.a, c.a, 0.0, -1.0, 0.0],
            # (3rd cell)
            [1 * c.d + c.a, -c.a, 0.0, -1.0, 0.0],
            [1 * c.d - c.a, c.a, 0.0, -1.0, 0.0],
            # (4th cell)
            [2 * c.d + c.a, -c.a, 0.0, -1.0, 0.0],
            [2 * c.d - c.a, c.a, 0.0, -1.0, 0.0]])


        QDs = np.array([  # driver cell:
                [-c.a - 2 * c.d, c.a, 0.0, 0.5, c.QD_radius],
                [c.a - 2 * c.d, c.a, 0.0, -0.5, c.QD_radius],
                [c.a - 2 * c.d, -c.a, 0.0, 0.5, c.QD_radius],
                [-c.a - 2 * c.d, -c.a, 0.0, -0.5, c.QD_radius],
                # 1st cell:
                [-c.a - c.d, c.a, 0.0, 0.5, c.QD_radius],
                [c.a - c.d, c.a, 0.0, 0.5, c.QD_radius],
                [c.a - c.d, -c.a, 0.0, 0.5, c.QD_radius],
                [-c.a - c.d, -c.a, 0.0, 0.5, c.QD_radius],
                # 2nd cell:
                [-c.a, c.a, 0.0, 0.5, c.QD_radius],
                [c.a, c.a, 0.0, 0.5, c.QD_radius],
                [c.a, -c.a, 0.0, 0.5, c.QD_radius],
                [-c.a, -c.a, 0.0, 0.5, c.QD_radius],
                # 3rd cell:
                [-c.a + 1 * c.d, c.a, 0.0, 0.5, c.QD_radius],
                [c.a + 1 * c.d, c.a, 0.0, 0.5, c.QD_radius],
                [c.a + 1 * c.d, -c.a, 0.0, 0.5, c.QD_radius],
                [-c.a + 1 * c.d, -c.a, 0.0, 0.5, c.QD_radius],
                # 4th cell:
                [-c.a + 2 * c.d, c.a, 0.0, 0.5, c.QD_radius],
                [c.a + 2 * c.d, c.a, 0.0, 0.5, c.QD_radius],
                [c.a + 2 * c.d, -c.a, 0.0, 0.5, c.QD_radius],
                [-c.a + 2 * c.d, -c.a, 0.0, 0.5, c.QD_radius]
            ])
        return els, QDs

    def generate_random_system(self):

        # indices of particle:[0,            1,   2,   3,      4     ]
        # description of idx: [x,            y,   z,   charge, radius]
        el_num = np.random.randint(1, 10)
        els = np.empty((el_num, 5), dtype=np.float_)
        for i in range(el_num):
            coord = np.random.rand(3) * 10
            els[i, :] = [*coord, -1.0, 0.0]

        QD_num = np.random.randint(1, 10)
        QDs = np.empty((QD_num, 5), dtype=np.float_)
        for i in range(QD_num):
            coord = np.random.rand(3) * 10
            QDs[i, :] = [*coord, -1.0, 0.0]

        return els, QDs


if __name__ == '__main__':
    unittest.main()
