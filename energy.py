import numpy as np


def total_energy(els, QDs):
    """
    Calculates total energy of a system, that consists of quantum dots (QD) and
    electros. Assuming nanometer as a unit of length and elementary charge as a
    unit of charge. This is the function in which most the of execution time is
    being spent.

    Args:
        els:                electrons (dynamic and moving particles)
        QDs:                Quantum dots, (fixed in place)

    Return:
        Configuration energy in Joules.
    """

    # How to spot a code smell?
    # When comments have to be written to remind self what indices of array are.
    #
    # Ok, bare numpy array is used (instead of python object) for easier FFI.
    # indices of particle:      0     1     2     3       4
    #              np.ndarray: [x,    y,    z,    charge, radius]

    # constants SI
    eps0 = 8.854187817e-12
    pi = 3.1415926535
    dielectric = 1.0
    k = 1 / (4 * pi * eps0 * dielectric)  # 1/ (4*pi*eps)
    q_e = 1.60217662e-19

    # convert all particles to one big list
    num_els = els.shape[0]
    num_QDs = QDs.shape[0]
    A = np.concatenate((els, QDs), axis=0)  # All particles

    Et = 0.0  # Total energy

    for i in range(num_els):  # skip all QD-QD -interactions (=constant)
        for j in range(i + 1, num_els + num_QDs):
            dist = np.sqrt((A[i, 0] - A[j, 0]) ** 2 +
                           (A[i, 1] - A[j, 1]) ** 2 +
                           (A[i, 2] - A[j, 2]) ** 2)

            R = max(A[i, 4], A[j, 4])  # larger radius of two

            if (dist < R):  # electron is inside quantum dot
                Et += k * A[i, 3] * A[j, 3] / (2 * R) * \
                      (3 - dist ** 2 / R ** 2) * (q_e ** 2 / (1e-9))

            else:  # neither is quantum dot, OR they are far enough not to matter
                Et += k * A[i, 3] * A[j, 3] / (dist) * (q_e ** 2 / (1e-9))
                # ^^^^ unit conversion

    return Et  # /q_e


def total_energy_vectorized(els, QDs):
    """
    Modified version of 'total_energy' so that one loop is totally replaced by
    numpy loop vectorization. A huge thanks to Juha Jeronen for the idea and
    code.

    Args:
        els:                electrons (dynamic and moving particles)
        QDs:                Quantum dots, (fixed in place)

    Return:
        Configuration energy in Joules.
    """

    # indices of particle:      0     1     2     3       4
    #              np.ndarray: [x,    y,    z,    charge, radius]

    # constants SI
    eps0 = 8.854187817e-12
    pi = 3.1415926535
    dielectric = 1.0
    k = 1 / (4 * pi * eps0 * dielectric)  # 1/ (4*pi*eps)
    q_e = 1.60217662e-19

    # convert all particles to one big list
    num_els = els.shape[0]
    num_QDs = QDs.shape[0]
    A = np.concatenate((els, QDs), axis=0)  # All particles

    Et = 0.0  # Total energy

    for i in range(num_els):  # skip all QD-QD -interactions (=constant)
        # Explisit broadcasting with np.tile(), because implicit broadcasting or
        # np.broadcast_to() is unclear in functioning to coder.
        A_broadcast_coord = np.tile(A[i, :3], ((A.shape[0] - i - 1), 1))
        A_part = A[i + 1:]  # all particles from i to end

        diffs = (A_part[:, :3] - A_broadcast_coord)
        dists_squared = (np.sum(diffs * diffs, axis=1))
        Rs = np.maximum(A_part[:, 4], A[i, 4])  # Larger radius of two

        in_QD = (
                    dists_squared < Rs ** 2)  # logical mask for electrons inside QDs
        Rs_in_QD = Rs[in_QD]  # create view just once (we need this twice)
        out_QD = np.logical_not(in_QD)

        # sum of electrons inside
        Et += np.sum(k * A[i, 3] * A_part[in_QD, 3] / (2 * Rs_in_QD) *
                     (3 - dists_squared[in_QD] / Rs_in_QD ** 2) *
                     (q_e ** 2 / (1e-9)))

        # sum of electrons outside
        Et += np.sum(k * A[i, 3] * A_part[out_QD, 3] /
                     np.sqrt(dists_squared[out_QD]) * (
                             q_e ** 2 / (1e-9)))

    return Et  # /q_e
