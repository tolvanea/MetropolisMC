import numpy as np

def cell_polarizations(state, conf) -> np.ndarray:
    """
    Brief
        Calculates polarizations for each cell from electron positions
    Return
        Polarizations for each cell.
        e.g. for two cell system: -> np.array([1, -1])
    """
    
    # electron occupation of quantum dots
    occs = np.full((len(state.cell_center_points), 4), fill_value=0)
    pols = np.full(len(state.cell_center_points), fill_value=-9999)
    
    for i, el in enumerate(state.els):
        
        for j, center in enumerate(state.cell_center_points):
            
            dx = el[0] - center[0]
            dy = el[1] - center[1]
            dz = el[2] - center[2]
            
            # electron is in cell
            if ((abs(dx) < 2*conf.a) and (abs(dy) < 2*conf.a) and (abs(dz) < conf.a)):
                to_idx_of_4 = lambda x, y: abs(int(x>0) - 3 * int(y<0))
                occs[j, to_idx_of_4(dx, dy)] += 1
                
    for j, o in enumerate(occs):
        sum = o[0] + o[1] + o[2] + o[3]
        if (sum != 0):
            P = int(-(o[0] + o[2] - o[1] - o[3]) / (sum))
        else:
            P = 0
        
        pols[j] = P
        
    return pols
