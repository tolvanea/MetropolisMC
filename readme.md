# MetropolisMC


## Abstract
This project contains simple Metropolis Monte Carlo toy program written in python. It simulates classical thermodynamic equilibrium in small quantum dot system. Python optimization-methods are applied and compared. This project is part of TUT course RAK-19006 Scientific computing in python.

_TLDR: Numba was the optimization method with least effort and the highest performance._

## Usage
Program can be run with 
```python3 main.py ```
which prints and plots result. Notice: It is higly recommended to use anaconda-python, since there are number of depedencies: (numpy, matplotlib, numba, cython). 

#### (Optional: Rust extra installation)
Also, there's one test (disabled by default) that uses compiled shared library written in Rust. It may be possible to run the test with pre-compiled lib-file, but if that fails, it must be compiled. Commands to compile Rust-file are:
```
    cd python-rust/
    sudo apt-get install cargo
    cargo install ndarray
    cargo build --release
```
However, Rust package manager (cargo) is quite large (over 200 MB), so it's not recommended to install it just to run single test. By default rust-test is disabled, it can be enabled from `main.py`.

## Introduction
### What is Metropolis-algorithm?
[Metropolis algorithm](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm) is a method to create samples from probability distributions that has unknown normalization. In this project, Metropolis-algorithm is used to sample thermodynamic distribution of electrons in quantum dots. Method is based on Markov chain random walker, which tries to find and sample low energy states. The algorithm is almost trivial, but it can be a challenging to make it accurate with finite computation resources. However, the system in this project is quite simple, and random walker has lower probability to get stuck on local minima.

### What is Quantum-dot cell?
A quantum-dot (QD) is electrically a charged region, purpose of which is to hold a single electron. Quantum-dot cell is structure of 4 QDs, that can fit 2 electrons in. A QD-cell has a purpose to represent two different states: polarization +1 or -1, which are formed by electrons occupying different corners of cell. [Quantum-dot cellular automata, QCA,](https://en.wikipedia.org/wiki/Quantum_dot_cellular_automaton) is a tehchology under research that uses polarizations to represent binary information. 

More about the subject can be read from my [bachelor thesis](https://gitlab.com/tolvanea/kandi/blob/master/runko.pdf) (in Finnish).

## Simulation 
**(Note: This project is more a proof of concept than accurate simulating software. Optimization methods have larger weight.)**

This toy program simulates a system consisting 4 quantum dot cells (classical interactions). The system is initially set in unbalanced state, which is local minumum. Walker can climb up local minimum (with minor tweaks in algorithms), and find a real energical minimum. (Therefore results can have some realistic sense in them.) The number of random samples (walker path length) is small, and so results have high error margin. Increasing `ConfigurationParameters.num_samples` in code can raise accuracy, but it brings longer run times.

Here is a picture a system and what electron density looks like with proper quantum mechanical simulation tools (PIMC, picture is from my bachelors thesis)

![images/electron_density_PIMC.png](images/electron_density_PIMC.png)

Here is a picture of electron density calculated with this toy program.

![images/electron_density_from_this_prject.png](images/electron_density_from_this_prject.png)

(I do not know why electron density is so localized in the centrums of quantum dots. It's probably some bug, or difference in density normalization.)

If this toy program is compared to PIMC, cell polarizations are correct at least in 10% margin. However, better results require longer simulation time (and more motivation).

## Optimization
When the python implementation is profiled, it is revealed that 95% of all time is spent in one simple energy calculation function. Major speedups are achieved by optimizing that single function. (For example: Numba-JIT drops the time cost of the function from 95% to 5%.)

This program can be profiled for example with `pyprof2calltree` and `kcachegrind` (call count), or `vprof` (line number execution).

### Different methods:
I used four different methods to optimize regular python implementation. The table below show the used methods and how long learning of that method took. Time notation on Numba _"30min + 3min"_ means that it took about 30 minutes to read documentation and get the implementaton to work first time. However, I estimate implementation time to be 3 minutes in the future.

| Optimization            | Effort (learn + use) | Comment               |
| :---------------------- |:-------------------- | :-------------------- |
| 1. Bare numpy python    | -                    |                       |
| 2. Parallel thread pool | 10min + 1 min        | (super easy)          |
| 3. Numba                | 30min + 1 min        | (really easy)         |
| 4. Cython               | 3h    + 3min         | (few tweaks)          |
| 5. Rust C FFI           | 10h   + 30min        | (I learned a lot)     |
| 5. Numpy vectorization  | 1h    + 30min        | (code by Juha Jeronen)|

#### Parallel thread pool
Pythons multiprocessing-library makes it really easy to divide repetive tasks between the processing cores. Multiple random walkers are easy to execute in parallel threads.

**UPDATE:** Initializing different seed for every thread fixed problem of identical runs.

#### Numba
Numba was ridiculously easy to get working (with anaconda python), and only few lines had to be added in the code. To push optimization even further, no-python mode was used, which required some minor modifications in original energy-function.

#### Cython
Cython took little patience to learn and to get working. Adding types was not a big problem, but compilation error messages were sometimes a bit unclear. (Also internet resources were not as abudant as it is with with regular python. However, thanks to [1](https://github.com/Technologicat/python-3-scicomp-intro).) 

**UPDATE:** Cython implementation speeded up ten folds by using cython min/max instead of numpy's versions. Python division caused only about 10% slowdown compared to c-division. Nogil version with parallel for loop did not increase efficiency. (It seems that on my computer 'cython.parallel.prange' does not use multiple threads.)

#### Rust with C FFI
The most interesting part was to try call compiled libraries with a C FFI. I used language called Rust, which is similar to c++, but which have fixed a ton of annoying (and stupid) quirks in C/C++. Also, rust has adopted few really nice modern features orginating from functional programming languages. 

Implementation took some time, because first I had to learn some Rust, and then I had to figure how to make it compile a dynamic library. The hardest part was pointer-passing of numpy arrays. (I had no previous knowledge about FFI:s. Also there aren't much documentation nor examples in the internet. To be honest, this FFI-implementation is quite a hack, so it has probably quite some extra overhead.)

#### UPDATE: Loop vectorization with Numpy operations
Clever numpy operations was tested to eliminate inner for-loop in energy calculations. The idea orginated from Juha Jeronen and all credits goes to him. Vectorized code is not the easiest to perceive and it took some time to understand its operation. However, it was good experience of handling not-so-naive loops.



## Optimization results
Major speedups were achieved with little effort. Following bar chart show the running times of full simulations. The slowest was unoptimized python, which took two minutes. This can be compared to Numba, which completes simulation in just 5 seconds. As stated before, un-optimized implementation spends 95% of all time in one single function, Numba-optimization removes that bottle neck almost entirely by lowerin proportional precentage to just 5%. If profiling results are reliable, then that single function runs now about 350 times faster. As it can bee seen from figure below, Numba's single JIT-function makes the full simulation run about 25 times faster.

![wow, so much fast](images/results/compare_running_times.png)

It is worth to notice, that parallelism was used only in one of optimizations. Numba, cython and rust -implementations could leverage to even better results with multithreaded walkers. Also there exists parallel for-loops.

## Summary
The verdict of this project is clear: _Optimization of a simple numerical function is suprisingly easy, and speedups are enormous._ Python multithreading pool is easy and quite effective, but it was experienced that parallel data sharing can cause problems. If software-depedencies of Numba are fulfilled, it is by far easiest and most effective way to optimize small python functions. Cython provides a good control of low level aspects of C (and C-interface). Rust (or C/C++) fits in situation better in those cases, when larger chunks of code will be optimized. 

All in all, this project was quite a lot of fun to do. I got practical touch with Mertropolis-algorithm, which is very simple but still effective and universal. Also, it was exciting to explore and experiment with different optimization methdos. The knowledge this project introduced will certainly be benefical in the future.







