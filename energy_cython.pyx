import numpy as np
import ctypes
cimport numpy as np
cimport cython
cimport cython.parallel
from libc.math cimport sqrt as c_sqrt
from cminmax cimport cfmax


"""
Cython optimization of energy-function.

Implementation is based on examples in:
https://github.com/Technologicat/python-3-scicomp-intro/

This cython file is compiled automaticly in main.py.
(The command to compile is: "python3 setup.py build_ext --inplace")
"""

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.cdivision(True)    # Use c division operation instead of python (!!!)
@cython.nonecheck(False)
@cython.profile(True)
def total_energy(double [:,::1] els, double [:,::1] QDs):
    """
    Calculates total energy of a system, that consists of quantum dots (QD) and electros.
    Assuming nanometer as a unit of length and elementary charge as a unit of charge.

    Args:
        els:                electrons (dynamic and moving particles)
        QDs:                Quantum dots, (fixed in place)

    Return:
        Configuration energy in Joules.

    """

    # cell consists of 6 particles
    #   ind:s of particle      0     1     2     3       4
    #        np.ndarray       [x,    y,    z,    charge, radius]

    # constanst SI
    cdef double eps0 = 8.854187817e-12
    cdef double pi = 3.1415926535
    cdef double dielectric=1.0
    cdef double k = 1/(4*pi*eps0*dielectric)  # 1/ (4*pi*eps)
    cdef double q_e = 1.60217662e-19

    # convert all particles to one big list
    cdef int num_els = els.shape[0]
    cdef int num_QDs = QDs.shape[0]
    # I could have used memory view "double[::]" or similar
    cdef double [:,::1] A = np.concatenate((els, QDs), axis=0)  # All particles
            
    cdef double Et = 0.0  # Total energy
    cdef double dist = 0.0
    cdef double R = 0.0
    
    cdef int i, j

    for i in range(num_els):  # skip all QD-QD -interactions (=constant)
        for j in range(i+1, num_els+num_QDs):
            dist = c_sqrt((A[i,0]-A[j,0])**2 + (A[i,1]-A[j,1])**2 + (A[i,2]-A[j,2])**2)

            R = cfmax(A[i,4], A[j,4])  # larger radius
            
            #  electorn is inside quantum dot
            if (dist < R):  
                Et += k*A[i,3]*A[j,3]/(2*R) * (3 - dist*dist/R**2) * (q_e*q_e / (1e-9))
            # neither is quantum dot, OR they are far enough not to matter
            else:
                Et += k * A[i,3] * A[j,3] / (dist) * (q_e*q_e / (1e-9))  # last term is for unit conversion
                    
    return Et  # /q_e


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.cdivision(True)    # Use c division operation instead of python (!!!)
@cython.nonecheck(False)
@cython.profile(True)
def total_energy_parallel(double [:,::1] els, double [:,::1] QDs):
    """
    Calculates total energy of a system, that consists of quantum dots (QD) and electros.
    Assuming nanometer as a unit of length and elementary charge as a unit of charge.

    Args:
        els:                electrons (dynamic and moving particles)
        QDs:                Quantum dots, (fixed in place)

    Return:
        Configuration energy in Joules.

    """

    # cell consists of 6 particles
    #   ind:s of particle      0     1     2     3       4
    #        np.ndarray       [x,    y,    z,    charge, radius]

    # constanst SI
    cdef double eps0 = 8.854187817e-12
    cdef double pi = 3.1415926535
    cdef double dielectric=1.0
    cdef double k = 1/(4*pi*eps0*dielectric)  # 1/ (4*pi*eps)
    cdef double q_e = 1.60217662e-19

    # convert all particles to one big list
    cdef int num_els = els.shape[0]
    cdef int num_QDs = QDs.shape[0]
    # I could have used memory view "double[::]" or similar
    cdef double [:,::1] A = np.concatenate((els, QDs), axis=0)  # All particles

    cdef double Et = 0.0  # Total energy
    cdef double dist = 0.0
    cdef double R = 0.0

    cdef int i, j

    ##############################################
    # NOTICE ONLY DIFFERENCE IS FOLLOWING TWO LINES
    with nogil:
        for i in cython.parallel.prange(num_els):  # skip all QD-QD -interactions (=constant)
        ##############################################
            for j in range(i+1, num_els+num_QDs):
                dist = c_sqrt((A[i,0]-A[j,0])**2 + (A[i,1]-A[j,1])**2 + (A[i,2]-A[j,2])**2)

                R = cfmax(A[i,4], A[j,4])  # larger radius

                #  electorn is inside quantum dot
                if (dist < R):
                    Et += k*A[i,3]*A[j,3]/(2*R) * (3 - dist*dist/R**2) * (q_e*q_e / (1e-9))
                # neither is quantum dot, OR they are far enough not to matter
                else:
                    Et += k * A[i,3] * A[j,3] / (dist) * (q_e*q_e / (1e-9))  # last term is for unit conversion

    return Et  # /q_e
