import numpy as np
import matplotlib.pyplot as plt
import sys
from timeit import default_timer as timer

import MetropolisMC as mc
import plotting
import energy
from multiprocessing import Pool
from copy import deepcopy

def summary_string(results, timediff, ident="    "):
    """
    Brief:
        Return string that contains essential information, and which will 
        be printed on screen.
    
    Args:
        results     : observables for each walker
        timediff    : measured time of execution
        ident       : text format identing
    """
    pol1_mean = 0.0
    pol4_mean = 0.0
    for res in results:
        pol1_mean += res.P1_mean
        pol4_mean += res.P4_mean
    pol1_mean /= len(results)
    pol4_mean /= len(results)
    
    row1 = ident + "Time : {:.2f}".format(timediff)
    row2 = ident + "Polarizations, cell 1 : {:.2f}, cell 4 = {:.2f}"\
                    .format(pol1_mean, pol4_mean)
    
    return "\n".join((row1, row2))
    
    
def run(energy_function, process="serial", save_name="unnamed.pdf", 
               num_random_walkers=6, plot=True):
    """Run simulation. Spawns multiple random walkers (serial or parallel).
    """
    check_valid_process_case(process)

    params = []
    for i in range(num_random_walkers):
        conf = deepcopy(mc.ConfigurationParameters())
        params.append([mc.State(conf), conf, energy_function, i])
    
    start_time = timer()
    
    # Normal serial loop of walkers
    if (process == "serial"):
        results = [mc.metropolis_loop(*param) for param in params]
    
    # parallel loop of walkers
    elif (process == "parallel"):
        with Pool(6) as p:
            results = p.starmap(mc.metropolis_loop, params)

    end_time = timer()
    print(summary_string(results, end_time - start_time), end="\n\n")
    if plot:
        plotting.plot_walkers(results, save_name)

def check_valid_process_case(process):
    """Check that prosess name is valid.
    """
    valid_cases = ("serial", "parallel")
    if process not in valid_cases:
        raise ValueError("Unknown case '{}', valid: {}"
                         .format(process, ", ".join(valid_cases)))


def case1_serial(num_random_walkers=6, plot=True):
    """
    Brief:
        Most simple serial implementation, that uses just bare numpy arrays.
        
    Args:
        num_random_walkers  : number of independent random walkers
        plot                : plot figures
    """
    
    print("Case 1: serial")
    
    try:
        run(energy.total_energy, process="serial", save_name="fig1_serial.pdf",
            num_random_walkers=num_random_walkers, plot=plot)
            
    except ImportError as err:
        print("    Could not load internal files:")
        print("        {}".format(err))
    
    except:
        print("    Something else went wrong:")
        print("        {}".format(sys.exc_info()[0]))
        raise
    
    print("")
        
        
def case2_parallel(num_random_walkers=6, plot=True):
    """Crete parallel multiprocessing thread pools for each walker.
    """
        
    print("Case 2: Parallel-multiprocessing-ppl")
    print("## (Notice! Results show that it really calculates only one walker multiple times!) ##")
    
    try:
        run(energy.total_energy, process="parallel", save_name="fig2_parallel.pdf",
            num_random_walkers=num_random_walkers, plot=plot)
    
    except ImportError as err:
        print("    Could not load internal files:")
        print("        {}".format(err))
    
    except:
        print("    Something else went wrong:")
        print("        {}".format(sys.exc_info()[0]))
        raise
    
    print("")
        

def case3_numba_serial(num_random_walkers=6, plot=True):
    """Use numba to jit-compile energy function. (Serial)
    """
    
    print("Case 3: Numba (serial)")
    
    try:
        # Following three lines are added compared to original imlementation:
        import numba as nb  
        energy_jit = nb.jit(nb.double(nb.double[:, :], nb.double[:, :]),
                            nopython=True)(energy.total_energy)
        
        run(energy_jit, process="serial", save_name="fig3_numba_serial.pdf",
            num_random_walkers=num_random_walkers, plot=plot)
            
    except ImportError as err:
        print("    Could not load numba:")
        print("        {}".format(err))
    
    except:
        print("    Something else went wrong:")
        print("        {}".format(sys.exc_info()[0]))
        raise
    
    print("")
    

def case4_cython_serial(num_random_walkers=6, plot=True):
    """Use cython to compile energy function. (Serial)
    """
    
    print("Case 4: Cython (serial). Compiling 'energy_cython.pyx'")
    
    try:
        import subprocess
        
        # Note automaticly compile cython:
        # python3 setup.py build_ext --inplace
        result = subprocess.run(['python3', 'setup.py', 'build_ext', '--inplace'],
                                stdout=subprocess.PIPE)
        #print("------------------\n",result.stdout,"\n------------------")
        
        import energy_cython

        run(energy_cython.total_energy, process="serial", save_name="fig4_cython_serial.pdf",
            num_random_walkers=num_random_walkers, plot=plot)
            
    except ImportError as err:
        print("    Could not load Cython:")
        print("        {}".format(err))
    
    except:
        print("    Something else went wrong:")
        print("        {}".format(sys.exc_info()[0]))
        raise
    
    print("")
      
    
def case5_rust_serial(num_random_walkers=6, plot=True):
    """Use C FFI to link shared library that is compiled with Rust.
    """
    
    print("Case 5: Rust (serial)")
    
    try:
        import rust_caller
        
        caller = rust_caller.RustEnergyCaller()
        
        run(caller.run, process="serial", save_name="fig5_rust_serial.pdf",
            num_random_walkers=num_random_walkers, plot=plot)
            
    except ImportError as err:
        print("    Could not load C FFI:")
        print("        {}".format(err))
    
    except:
        print("    Something else went wrong, which is expected with FFI:s:")
        print("        {}".format(sys.exc_info()[0]))
        raise
    
    print("")


def case6_numpy_vectorized(num_random_walkers=6, plot=True):
    """ Case 6: For loops optimized by numpy vectorization.
    """

    print("Case 6: Numpy vectorization.")

    try:
        run(energy.total_energy_vectorized, process="serial",
            save_name="fig6_numpy_vec.pdf",
            num_random_walkers=num_random_walkers, plot=plot)

    except ImportError as err:
        print("    Could not load internal files:")
        print("        {}".format(err))

    except:
        print("    Something else went wrong:")
        print("        {}".format(sys.exc_info()[0]))
        raise

    print("")


def main():
    """
    Brief:
        Call all differently optimized versions of Meropolis algorithm.
    """
    
    num_random_walkers = 6
    plot = True           
    
    print("Running metropolis algorithm. Expected computing time is about a five minutes.")

    # Normal python (numpy arrays)
    case1_serial(num_random_walkers, plot)
    case2_parallel(num_random_walkers, plot)

    # Numba optimization
    case3_numba_serial(num_random_walkers, plot)

    # Cython optimization
    case4_cython_serial(num_random_walkers, plot)

    # Change to True if: 
    #       - you accept that it may fail to use pre-compiled dynamic library
    #       OR
    #       - you bothered to install Rust compiler, and compiled the source
    # (see readme.md for more info)
    I_understand_this_may_fail = False
    
    # Use rust compiled binary
    if I_understand_this_may_fail:
        case5_rust_serial(num_random_walkers, plot)

    case6_numpy_vectorized(num_random_walkers, plot)

    if plot:
        plt.show()

main()
