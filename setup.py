from distutils.core import setup
from Cython.Build import cythonize 

"""
This file compiles cython. There's no need to run this file by hand, as it is 
automated in main.py.
"""

setup(
    ext_modules = cythonize("energy_cython.pyx")
)
