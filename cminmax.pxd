# -*- coding: utf-8 -*-
#
# All credits (and thanks) goes to Juha Jeronen!
#
# fast inline min/max functions for C code in Cython.

ctypedef double RTYPE_t  # real number type

# real
#
cdef inline RTYPE_t cfmin(RTYPE_t a, RTYPE_t b) nogil:
    return a if a < b else b
cdef inline RTYPE_t cfmax(RTYPE_t a, RTYPE_t b) nogil:
    return a if a > b else b

# int
#
cdef inline int cimin(int a, int b) nogil:
    return a if a < b else b
cdef inline int cimax(int a, int b) nogil:
    return a if a > b else b

# unsigned int
#
cdef inline unsigned int cuimin(unsigned int a, unsigned int b) nogil:
    return a if a < b else b
cdef inline unsigned int cuimax(unsigned int a, unsigned int b) nogil:
    return a if a > b else b
