import numpy as np
from cffi import FFI
import energy

"""
This file contains some sort of python wrapper to make rust-function easier to use.
"""

# Following code example helped me:
# https://users.rust-lang.org/t/calling-into-rust-from-python/8923/2
 

class RustEnergyCaller():
    """
    This class mimics a function with memory. It store FFI information on 
    initialization, and then remembers it afterwards.
    """
    def __init__(self):        
        self.ffi = FFI()
        self.ffi.cdef("""
                 double rust_entry_function(
                     double* v1_ptr, unsigned long v1_m, unsigned long v1_n,
                     double* v2_ptr, unsigned long v2_m, unsigned long v2_n);
                 """)
        
        # If compiled binary does not work, you can optionally compile it by:
        #       cd python-rust/
        #       sudo apt-get install cargo    # notice: it's over 200MB
        #       cargo install ndarray         # gives error: specified package has no binaries
        #       cargo build --release
        self.lib = self.ffi.dlopen("python-rust/target/release/libenergyrust.so")
    
        self.lib.rust_entry_function
        
    def run(self, els, QDs) -> float:
        """This function calls compiled shared library."""
        
        els_ptr = self.as_f64_array(els)      # pointer
        els_m   = self.as_usize(els.shape[0]) # rows
        els_n   = self.as_usize(els.shape[1]) # columns
        
        QDs_ptr = self.as_f64_array(QDs)
        QDs_m   = self.as_usize(QDs.shape[0])
        QDs_n   = self.as_usize(QDs.shape[1])
    
        # Call
        E = self.lib.rust_entry_function(els_ptr, els_m, els_n, 
                                         QDs_ptr, QDs_m, QDs_n)
        
        return E

    def as_f64_array(self, array):
        # Cast np.float64 array to a pointer to float 64s.
        return self.ffi.cast("double*", array.ctypes.data)
    
    def as_usize(self, num):
        # Cast `num` to Rust `usize`.
        return self.ffi.cast("unsigned long", num)
